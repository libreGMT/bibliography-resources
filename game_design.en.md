# Game design (en)

Game design resources, tutorials, link, repositories, etc.

## RPG

* [Blog - How to Make an RPG](https://howtomakeanrpg.com/)
* [Forum, guide - A guide to making a balanced game](https://www.rpgmakercentral.com/topic/4933-making-a-balanced-game/)
* [Forum - Mathematics of Game Design: Probability and Statistics](https://www.rpg.net/columns/physics/physics11.phtml)
* [Wiki - Damage calculation in Pokémon](https://bulbapedia.bulbagarden.net/wiki/Damage)
* [Forum - RPG attributes and statistics](https://forum.unity.com/threads/rpg-attributes-and-statistics.276292/)
