# Software Engineering (en)

Software engineering resources, tutorial, link, repositories, etc.

## Generic

## Languages specific

### Python

### Godot

* [Godot Unit Test. Unit testing tool for Godot Game Engine.](https://github.com/bitwes/Gut)
* [Repository to showcase how to setup Continuous Integration for your Godot game](https://github.com/dsaltares/godot-ci-example)